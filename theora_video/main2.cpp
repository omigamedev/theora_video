#include <ogg/ogg.h>
#include <theora/theoradec.h>
#include <GLFW/glfw3.h>
//#include <unistd.h>

#include "../soil/SOIL.h"

#include <map>
#include <mutex>
#include <cmath>
#include <deque>
#include <memory>
#include <vector>
#include <chrono>
#include <thread>
#include <fstream>
#include <algorithm>
#include <condition_variable>

#define OC_CLAMP255(_x)     ((unsigned char)((((_x)<0)-1)&((_x)|-((_x)>255))))

#define LOG(s,...) printf(s,##__VA_ARGS__)
#define LOG(s,...)

volatile int nx = 0, ny = 0;
int width = 800, height=800;
int NX = 1;
int NY = 1;

struct Packet
{
    ogg_packet m_pkt;

    Packet(ogg_packet& p)
    {
        m_pkt = p;
        m_pkt.packet = new unsigned char[p.bytes];
        memcpy(m_pkt.packet, p.packet, p.bytes);
    }
    
    ~Packet()
    {
        ogg_packet_clear(&m_pkt);
        LOG("destroy packet\n");
    }
};

class Stream
{
    friend class Decoder;
    
public:
    th_info          m_info;
    th_comment       m_comment;
    th_setup_info*   m_setup;
    th_dec_ctx*      m_decoder;
    ogg_stream_state m_stream;
    std::deque<std::shared_ptr<Packet>> m_queue;
    
    bool m_ended;
    int m_count;
    int m_pkt_ready;
    std::thread m_thread;
    GLuint m_textureID;
    
public:
    Stream(int serial)
    {
        th_comment_init(&m_comment);
        th_info_init(&m_info);
        ogg_stream_init(&m_stream, serial);
        m_setup = nullptr;
        m_count = 0;
        m_ended = false;
        m_textureID = 0;
    }
    
    ~Stream()
    {
//        glDeleteTextures(1, &m_textureID);
// TODO: clean up
    }
};

class Decoder
{
public:
    std::map<int,std::shared_ptr<Stream>> m_streams;
    std::vector<int> m_keys;
    std::ifstream   m_file;
    ogg_sync_state  m_sync_state;
    th_ycbcr_buffer m_ycbcr;
    th_info         m_info;
    unsigned char*  m_rgb;
    
    std::thread m_thread;
    std::mutex  m_mutex;
    std::condition_variable m_cv;

    int m_nthreads;
    std::condition_variable m_wcv;
    std::mutex m_wmutex;
    std::condition_variable m_dcv;
    std::mutex m_dmutex;
    std::vector<std::thread> m_workers;
    std::deque<int> m_wserials;
    std::vector<std::vector<unsigned int>> m_wbuffers;

public:
    int width;
    int height;
    int streams;
    bool eos;
    bool running;
    
    ~Decoder()
    {
        m_thread.join();
    }
    
    Stream* stream(int index)
    {
        return m_streams[m_keys[index]].get();
    }
    
    bool open(const char* filename)
    {
        m_file.open(filename, std::ios::binary);
        if (!m_file.is_open())
            return false;
        
        ogg_sync_init(&m_sync_state);
        if (!(_parse_bos() && _parse_headers()))
            return false; // TODO: clear
        
//        th_dec_ctx* ctx = m_streams.begin()->second->m_decoder;
//        int pp_max;
//        th_decode_ctl(ctx, TH_DECCTL_GET_PPLEVEL_MAX, &pp_max, sizeof(pp_max));
//        th_decode_ctl(ctx, TH_DECCTL_SET_PPLEVEL, &pp_max, sizeof(pp_max));
                
        m_info = m_streams.begin()->second->m_info;
        m_rgb = new unsigned char[width * height * 3];
        streams = (int)m_streams.size();
        
        return true;
    }
    
    void run(int nthreads)
    {
        m_nthreads = nthreads;
        running = true;
        m_thread = std::thread(&Decoder::_read_thread, this);
        for (int i = 0; i < m_nthreads; i++)
        {
            m_wbuffers.emplace_back(width * height * 3);
            m_workers.emplace_back(&Decoder::_decode_worker, this);
        }
    }
    
    void _decode_worker()
    {
        
        while (true)
        {
            std::unique_lock<std::mutex> lock(m_wmutex);
            m_wcv.wait(lock, [&]{
                // returns ​false if the waiting should be continued
                if(m_wserials.size() > 0)
                    return true; // stop waiting and work
                return false; // wait
            });
            int serial = m_wserials.back();
            m_wserials.pop_back();
            lock.unlock();
            m_wcv.notify_all();
            
            // decode
            _decode_stream(serial);
//            Stream* s = m_streams[serial].get();
//            th_decode_ycbcr_out(s->m_decoder, m_ycbcr);
//            _yuv2rgb();
//            
//            glBindTexture(GL_TEXTURE_2D, s->m_textureID);
//            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
//                GL_RGB, GL_UNSIGNED_BYTE, m_rgb);
            
            m_dcv.notify_all();
        }
    }
    
    void decode()
    {
        static int count = 0;
        std::unique_lock<std::mutex> lock(m_mutex);
        eos = false;
        m_cv.wait(lock, [&]
        {
            int min = 10;
            for (auto& s : m_streams)
            {
                min = std::min(min, static_cast<int>(s.second->m_queue.size()));
//                if (!s.second->m_stream.e_o_s)
//                    eos = false;
            }
            return min > 0;// || eos; // wait if some streams doesn't have a packet
        });
//        if (eos)
//        {
//            LOG("decode eos\n");
//            return;
//        }

        if (m_nthreads > 1)
        {
            std::unique_lock<std::mutex> wlock(m_wmutex);
            for (auto& s : m_streams)
                m_wserials.push_front(s.first);
            wlock.unlock();
            m_wcv.notify_all();
            
            std::unique_lock<std::mutex> dlock(m_dmutex);
            m_dcv.wait(dlock, [&]{
                // returns ​false if the waiting should be continued
                if(m_wserials.size() == 0)
                    return true; // stop waiting and work
                return false; // wait
            });
            dlock.unlock();
        }
        else
        {
            for (int i = 0; i < streams; i++)
            {
//                int vx = i % NX;
//                int vy = i / NX;
//                bool active = abs(vx - nx) <= 1 && abs(vy - ny) <= 1;
                _decode_stream(m_keys[i]);
            }
        }
        
        
        LOG("decode %d\n", count++);
        lock.unlock();
        m_cv.notify_all();
    }

    void convertRGB(int index)
    {
        auto s = stream(index);
        th_decode_ycbcr_out(s->m_decoder, m_ycbcr);
        _yuv2rgb();
        
        glBindTexture(GL_TEXTURE_2D, s->m_textureID);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
            GL_RGB, GL_UNSIGNED_BYTE, m_rgb);
    }
    
    void convertRGB()
    {
        for (auto& s : m_streams)
        {
            th_decode_ycbcr_out(s.second->m_decoder, m_ycbcr);
            _yuv2rgb();
            
            glBindTexture(GL_TEXTURE_2D, s.second->m_textureID);
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
                GL_RGB, GL_UNSIGNED_BYTE, m_rgb);
        }
    }
    
    void close()
    {
        if (m_file.is_open())
            m_file.close();
        // TODO: clear ogg
    }
    

private:
    
    bool _decode_stream(int serial)
    {
        ogg_int64_t pos;
        auto& s = m_streams[serial];
        auto packet = s->m_queue.back();
        int decoded = th_decode_packetin(s->m_decoder, &packet->m_pkt, &pos);
        if (decoded == 0)
        {
            double time = th_granule_time(s->m_decoder, pos);
            LOG("packet %d (id:%d) on stream %x at time %fs\n",
                s->m_count, (int)s->m_stream.packetno, serial, time);
            s->m_count++;
            
            if (th_packet_iskeyframe(&packet->m_pkt))
                LOG("keyframe\n");
        }
        else
        {
            LOG("ERROR packet %d (id:%d) on stream %x\n",
                s->m_count, (int)s->m_stream.packetno, serial);
        }
        //s->m_queue.pop_back();
        return decoded == 0;
    }
    
    long _read_file()
    {
        char* buffer = ogg_sync_buffer(&m_sync_state, 4096);
        m_file.read(buffer, 4096);
        ogg_sync_wrote(&m_sync_state, m_file.gcount());
        return m_file.gcount();
    }
    
    GLuint _createTexture(int w, int h)
    {
        GLuint tex_id;
        glGenTextures(1, &tex_id);
        glBindTexture(GL_TEXTURE_2D,tex_id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        return tex_id;
    }

    void _yuv2rgb()
    {
        for (int row = 0; row < height; row++)
        {
            unsigned char* ydata = &m_ycbcr[0].data[m_ycbcr[0].stride * row];
            unsigned char* udata = &m_ycbcr[1].data[m_ycbcr[1].stride * (row/2)];
            unsigned char* vdata = &m_ycbcr[2].data[m_ycbcr[2].stride * (row/2)];
            unsigned char* out = &m_rgb[width * (height-row-1) * 3];
            for (int x = 0; x < width; x++)
            {
                int x2 = x/2;
                // Julien forula from http://www.fourcc.org/fccyvrgb.php
                out[x * 3 + 0] = OC_CLAMP255(static_cast<int>(ydata[x] + 1.402f*(vdata[x2]-127)));
                out[x * 3 + 1] = OC_CLAMP255(static_cast<int>(ydata[x] - 0.344f*(udata[x2]-127) - 0.714f*(vdata[x2]-127)));
                out[x * 3 + 2] = OC_CLAMP255(static_cast<int>(ydata[x] + 1.722f*(udata[x2]-127)));
            }
        }
    }
    
    bool _parse_bos()
    {
        int ret;
        ogg_page page;
        ogg_packet packet;

        while ((ret = ogg_sync_pageout(&m_sync_state, &page)) >= 0)
        {
            if (ret == 0)
            {
                long bytes = _read_file();
                if (bytes == 0)
                    return false;
                continue;
            }
            
            int serial = ogg_page_serialno(&page);
            
            if (m_streams[serial].get() == nullptr)
            {
                m_streams[serial] = std::make_shared<Stream>(serial);
                m_keys.push_back(serial);
            }
            
            if (!ogg_page_bos(&page))
            {
                ogg_stream_pagein(&m_streams[serial]->m_stream, &page);
                return true;
            }
            
            ogg_stream_state test;
            ogg_stream_init(&test, serial);
            ogg_stream_pagein(&test, &page);
            int complete = ogg_stream_packetout(&test, &packet);
            if (complete == 1)
            {
                int header_result = th_decode_headerin(&m_streams[serial]->m_info,
                    &m_streams[serial]->m_comment, &m_streams[serial]->m_setup, &packet);
                if (header_result > 0)
                {
                    m_streams[serial]->m_stream = test;
                }
                LOG("bos header packet %d\n", (int)m_streams[serial]->m_stream.packetno);
            }
            else
            {
                ogg_stream_clear(&test);
            }
        }
        return false;
    }

    bool _parse_headers()
    {
        ogg_page page;
        ogg_packet packet;
        int done = 0;
        int ret;
        while (done < m_streams.size() && (ret = ogg_sync_pageout(&m_sync_state, &page)) >= 0)
        {
            if (ret == 0)
            {
                long bytes = _read_file();
                if (bytes == 0)
                    return false;
                continue;
            }
            
            int serial = ogg_page_serialno(&page);
            
            ogg_stream_pagein(&m_streams[serial]->m_stream, &page);
            int complete = ogg_stream_packetpeek(&m_streams[serial]->m_stream, &packet);
            if (complete)
            {
                int header_result = th_decode_headerin(&m_streams[serial]->m_info,
                    &m_streams[serial]->m_comment, &m_streams[serial]->m_setup, &packet);
                if (header_result == 0)
                {
                    m_streams[serial]->m_decoder = th_decode_alloc(&m_streams[serial]->m_info,
                        m_streams[serial]->m_setup);
                    LOG("initialized stream %x with packet %d\n", serial,
                        (int)m_streams[serial]->m_stream.packetno);
                    LOG("- vendor: %s\n", m_streams[serial]->m_comment.vendor);
                    for (int i = 0; i < m_streams[serial]->m_comment.comments; i++)
                    {
                        LOG("- comments %d: %.*s\n", i,
                            m_streams[serial]->m_comment.comment_lengths[i],
                            m_streams[serial]->m_comment.user_comments[i]);
                    }
                    
                    width = m_streams[serial]->m_info.frame_width;
                    height = m_streams[serial]->m_info.frame_height;

                    m_streams[serial]->m_textureID = _createTexture(width, height);

//                    th_info_clear(&m_streams[serial]->m_info);
                    th_comment_clear(&m_streams[serial]->m_comment);
                    th_setup_free(m_streams[serial]->m_setup);
                    m_streams[serial]->m_setup = nullptr;
                    done++;
                }
                else
                {
                    ogg_stream_packetout(&m_streams[serial]->m_stream, nullptr);
                    LOG("header packet %d\n", (int)m_streams[serial]->m_stream.packetno);
                }
            }
        }
        return true;
    }
    
    void _read_thread()
    {
        ogg_page page;
        ogg_packet packet;
        int ret;
        while ((ret = ogg_sync_pageout(&m_sync_state, &page)) >= 0)
        {
            if (ret == 0)
            {
                long bytes = _read_file();
                if (bytes == 0)
                    break;
                continue;
            }
            
            int serial = ogg_page_serialno(&page);
            
            std::unique_lock<std::mutex> lock(m_mutex);
            m_cv.wait(lock, [&]
            {
                int min = 30;
                for (auto& s : m_streams)
                    min = std::min(min, static_cast<int>(s.second->m_queue.size()));
                
//                LOG("reader %s %s\n", empty?"produce":"wait",
//                    empty&&ogg_page_eos(&page)?"eos":"");
                
                // returns ​false if the waiting should be continued
                if (min > 5)
                    return false; // wait
                return true; // stop waiting
            });
            
            ogg_stream_pagein(&m_streams[serial]->m_stream, &page);
            while (ogg_stream_packetout(&m_streams[serial]->m_stream, &packet))
            {
                m_streams[serial]->m_queue.emplace_front(std::make_shared<Packet>(packet));
                LOG("reader packet\n");
                //ogg_packet_clear(&packet);
            }
            
            lock.unlock();
            m_cv.notify_all();
        }
        LOG("reader eof\n");
        m_cv.notify_all();
        running = false;
    }
};

void drawPlane()
{
//    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
    glTexCoord2f(1, 1);
    glVertex3f(1, 1, 0);
    
    glTexCoord2f(0, 1);
    glVertex3f(-1, 1, 0);
    
    glTexCoord2f(0, 0);
    glVertex3f(-1, -1, 0);
    
    glTexCoord2f(1, 0);
    glVertex3f(1, -1, 0);
    glEnd();
}

void mouse(GLFWwindow* wnd, double x, double y)
{
    if (x < 0) x = 0;
    if (x > width) x = width-1;
    if (y < 0) y = 0;
    if (y > height) y = height-1;
    nx = (int)x / (width / NX);
    ny = ((int)y % (height/2)) / ((height/2) / NY);
}

void keyb(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
            case GLFW_KEY_SPACE:
                break;
            default:
                break;
        }
    }
}

int main()
{
    GLFWwindow* window;

    glfwInit();
    window = glfwCreateWindow(width, height, "Theora Player", NULL, NULL);
    glfwSetKeyCallback(window, keyb);
    glfwSetCursorPosCallback(window, mouse);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);
    glClearColor(1, 0, 0, 1);

    // Go to perspective mode
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);


    Decoder dec;
    //    file.open("/Users/omimac/Desktop/multi-video/rc_complete_right.ogv");
    //    file.open("/Users/omimac/Desktop/multi-video/cubes1.ogv");
    //    file.open("/Users/omimac/Desktop/multi-video/left.ogv");4k_r_g25_mosaic16x8_3
    if (!dec.open("E:\\4k_r_g25_mosaic16x8_3.ogv"))
        return -1;
    dec.run(10);
    
    NY = sqrtf(ceilf(dec.streams * 0.5f));
    NX = sqrtf(dec.streams * 2);
    
    std::chrono::time_point<std::chrono::system_clock> begin, end;
    int frames = 0;
    while (!glfwWindowShouldClose(window)/* && !dec.eos*/)
    {
        glClear(GL_COLOR_BUFFER_BIT);
        
        begin = std::chrono::system_clock::now();
        dec.decode();
        for (int y = 0; y < NY; y++)
        {
            for (int x = 0; x < NX; x++)
            {
                bool active = abs(x - nx) <= 1 && abs(y - ny) <= 1;
                bool center = x == nx && y == ny;
                float w = 2.0f / NX;
                float h = 2.0f / NY;
                if (center)
                    glColor3f(1, 0, 0);
                else if (active)
                    glColor3f(1, 1, 1);
                else
                    glColor3f(0.3, 0.3, 0.3);
                
                //if (active)
                //    dec.convertRGB(x + y * NX);
                
                glViewport(0, height/2, width, height/2);
                glBindTexture(GL_TEXTURE_2D, dec.stream(x + y * NX)->m_textureID);
                glLoadIdentity();
                glTranslatef(-1 + (w/2) + (x * w), 1 - y*h - h/2, 0);
                glScalef(w/2, h/2, 1);
                drawPlane();
            }
        }
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = end - begin;
        printf("%f\n", diff.count());
        
        //if (frames++ > 100)
        //    exit(0);

//        glLoadIdentity();
//        drawPlane();
        
        glfwSwapBuffers(window);
        glfwPollEvents();
        //usleep(100000);
    }
    dec.close();


    return 0;
}
